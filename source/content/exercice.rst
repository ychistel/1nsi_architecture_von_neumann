Exercices
=========

.. _`darty informatique`: https://www.darty.com/nav/achat/informatique/ordinateur_portable-portable/portable/dell_lat7400_i7-8_16_256.html

.. exercice::

    On donne une capture d'écran d'un site de vente de matériel informatique.

    .. figure:: ../img/pc_dell.png
        :align: center

        `darty informatique`_

    Parmi les points forts affichés, quels sont les éléments de l'architecture de Von Neumann que l'on peut identifier ?

.. exercice::

    #.  Quelles sont les parties d'un ordinateur dans le modèle d'architecture de Von Neumann ?
    #.  Le CPU d'un ordinateur se décompose en 2 unités. Lesquelles ?
    #.  Quel est le rôle de l'UAL ?

.. exercice::

    Un ordinateur manipule de l'information codée en **binaire**, c'est à dire des **bits** qui valent 0 ou 1. Un programme et les données qui se trouvent en mémoire vive et dans le processeur sont codés en binaire. 
    
    #.  Comment appelle-ton une information codée sur 8 bits ?
    #.  Une mémoire d'ordinateur est constituée de cellules de 64 bits chacune. Combien faut-il de cellules au minimum pour placé en mémoire un programme de 256 bits ? 
    
.. exercice::

    Les capacités d'un ordinateur sont exprimées par des multiples d'octets : kilo-octet (Ko), mega-octet (Mo), giga-octet (Go) et tera-octet (To).

    #.  Exprimer les quantités suivantes avec le multiple le plus adapté.

        a.  2000 Ko
        b.  1 000 000 octets
        c.  750 000 Mo
        d.  0,05 To

    #.  En informatique, il existe une unité de mesure de la taille mémoire qui utilise les puissance de 2. Par exemple, le **kibi-octet (kio)** représente une taille mémoire de :math:`2^{10}` octets. On a aussi le **mebi-octet (mio)**, le **gibi-octet (gio)** et le **tebi-octet (tio)**.

        a.  Combien d'octets a-t-on dans 1 mio ? et dans 1 gio ?
        b.  A-t-on :math:`1kio = 1ko` ? Justifier.
        c.  Exprimer :math:`1gio` en giga-octet ?

.. exercice::

    Un ordinateur possède 2 disques durs (mémoire de masse) et une mémoire vive. On donne les vitesses d'accès à ces différentes mémoires:
    
    -   :math:`10 ms` pour le disque dur mécanique
    -   :math:`50\mu s` pour un disque dur de type SSD
    -   :math:`10ns` pour la mémoire vive.

    #.  Classer ces accès du plus lent au plus rapide.
    #.  Combien d'accès le processeur peut-il faire à la mémoire vive pendant un accès du processeur au disque de type SSD ?
    #.  Même question pour le disque dur mécanique.

.. exercice::

    L'écran d'ordinateur affiche des *pixels*, c'est à dire des points lumineux de couleur. Chaque pixel est représenté en mémoire par trois octets, et l'écran est composé de 1920 pixels horizontalement par 1080 verticalement (résolution HD).

    #.  Quelle est la taille mémoire nécessaire pour afficher une image à l'écran ?
    #.  En supposant que le processeur soit capable d'écrire en mémoire chaque pixel en 1 cycle d'horloge et que la vitesse de l'horloge est 1 Ghz:
    
        a.  Combien de temps faut-il pour afficher tout l'écran ?
        b.  Combien d'images plein écran peut-on afficher par seconde au maximum ?
